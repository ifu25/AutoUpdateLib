# AutoUpdateLib
-------------------
一个方便易用的 Android 自动升级类库。

项目原作者：MZCretin，https://github.com/MZCretin/AutoUpdateProject

对原项目进行了简单修改，适配 Android 8.0，感谢mzcretin。

特点概述

一、可从后台主动控制本地app强制更新，主要适用场合是某个版本有bug，会严重影响用户的使用，此时用这种模式，只要用户打开app，提醒强制更新，否则不能进入app；

二、根据后台返回受影响的版本号，可控制多个版本同时被强制更新；

三、后台返回最新安装包MD5，本地判断安装包是否下载，防止多次下载；

四、内部处理，忽略此版本更新提示

五、采用无第三方工具类，下载使用HttpURLConnextion，本地存储使用SharedPrefference，以免使用此library带来第三方插件冲突

六、适配Android 8.0

七、默认请求方式为POST请求，对于GET请求无法正常请求，提供配置请求方式

八、添加了对log日志的开关，在调试阶段可打开调试log输出，提交生产环境的时候可以主动的关闭

九、支持判断网络环境，移动数据下智能提示，防止流量消耗。

十、考虑到减少与后台人员的配合的高度耦合，增加了自定义model的入口，具体使用请关注使用方式。

十一、提供种模式的版本更新，一种是对话框显示下载进度，一种是通知栏显示后台默默下载形式

十二、提供了另外两种样式的更新界面

-------------------

## 使用方式：

-------------------

**Step 1.** Add repositories in build.gradle:
```gradle
repositories {
    flatDir {
        dirs 'libs'
    }
}
```

**Step 2.** Add the aar dependency
copy AutoUpdate_1.0.0.aar to libs directory and add dependency in build.gradle
```gradle
implementation(name: 'AutoUpdate_1.0.0', ext: 'aar')
```

**Step 3.** Init it in BaseApplication or MainActivity before using it.And then register BaseApplication in AndroidManifest(Don't forget it).There are two ways you can chose.

```
//第一种形式 自定义参数
AutoUpdateUtil.Builder builder =
		new AutoUpdateUtil.Builder()
		//设置更新api
		.setBaseUrl("http://www.xxx.com/autoupdate.json")
		//设置是否显示忽略此版本
		.setIgnoreThisVersion(true)
		//设置下载显示形式 对话框或者通知栏显示 二选一
		.setShowType(CretinAutoUpdateUtils.Builder.TYPE_DIALOG)
		//设置下载时展示的图标
		.setIconRes(R.mipmap.ic_launcher)
		//设置下载时展示的应用名称
		.setAppName("测试应用")
		.build();
AutoUpdateUtil.init(builder);

//第二种模式
//AutoUpdateUtil.init("http://www.xxx.com/autoupdate.json");
```

**Step 4.** Add below codes to your app module's AndroidManifest file where under tags.
```xml
<application
        android:name=".BaseApp"
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">
        <activity android:name=".MainActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>

                <category android:name="android.intent.category.LAUNCHER"/>
            </intent-filter>
        </activity>

        <service android:name="com.willcoo.autoupdatelib.utils.DownloadService"/>
    </application>
```

**Step 5.** Start using it wherever you want as below.

```
AutoUpdateUtil.getInstance(MainActivity.this).check();
```

-------------------
后台返回Json模板，这些字段都是必须的，相关说明请看注释，下面是一个参考
```json
{
    appCode: "myapp",
    appName: "自动升级演示版",
    versionCode: "9",
    versionName: "1.09.20180825",
    versionDate: "2018/08/25",
    apkSize: 5587575,
    apkMd5: "510175745A2EACDA83580964ED7D9D97",
    downloadUrl: "https://www.willcoo.com/app-new.apk",
    updateLog: "1、增加事件上报模块 2、增加自动升级功能 3、系统优化",
    isForceUpdate: "0",
    preBaselineCode: "0",
    hasAffectCodes: "1|2|3|4"
}
```

